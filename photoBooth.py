#PhotoBooth Script v1
#the Bear-o

#import libraries
import time
import picamera
import pygame
import config
import os
import atexit
from pygame.locals import*
#multithreading with gpio button and LED
import _thread
#import the GPIO and time package
import RPi.GPIO as GPIO

#initialize
#############################
### Variables that Change ###
#############################
# Do not change these variables, as the code will change it anyway
transform_x = config.monitor_w # how wide to scale the jpg when replaying
transfrom_y = config.monitor_h # how high to scale the jpg when replaying
offset_x = 0 # how far off to left corner to display photos
offset_y = 0 # how far off to left corner to display photos
replay_delay = 1 # how much to wait in-between showing pics on-screen after taking
replay_cycles = 2 # how many times to show each photo on-screen after taking
#picam
camera = picamera.PiCamera()
camera.vflip = False
camera.iso = config.camera_iso
##image functions
#pygame
pygame.init()
pygame.display.set_mode((config.monitor_w,config.monitor_h))
screen = pygame.display.get_surface()
pygame.display.set_caption('Photo Booth Pics')
pygame.mouse.set_visible(False)
pygame.display.toggle_fullscreen()
##################
##Load config.py values
##################
#paths to slides
slidesFolder = config.slidesFolder
introSlide = config.introSlide
instructionsSlide = config.instructionsSlide
processingSlide = config.processingSlide
finishedSlide = config.finishedSlide
slide1 = config.slide1
slide2 = config.slide2
slide3 = config.slide3
slide4 = config.slide4
#sound folder
sound_folder_path = config.sound_folder_path

sound_array = ['hippy','cowboy','announcer']
#GPIO PIN SETUP
RED_LED = config.RED_LED
START_BUTTON = config.START_BUTTON
EXIT_BUTTON = config.EXIT_BUTTON
TEST_BUTTON = config.TEST_BUTTON
UP_BUTTON = config.UP_BUTTON
DOWN_BUTTON = config.DOWN_BUTTON
#delays
BUTTON_DELAY =.25

SHUTDOWN_ENABLED = config.SHUTDOWN_ENABLED
file_path = config.file_path
NUMBER_OF_PICS = config.NUMBER_OF_PICS
PRINTER_ENABLED = config.PRINTER_ENABLED
PRINTER_NAME = config.PRINTER_NAME
#####
#global vaiables - multithread
#####
exit_status = False
test_status = False
#####
#GPIO SETUP
###
GPIO.setmode(GPIO.BOARD)
GPIO.setup(RED_LED, GPIO.OUT)
GPIO.setup(START_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(EXIT_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(TEST_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(UP_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(DOWN_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
#####
TEST_DELAY = 5
#ISO list Setup
iso_index = 0
iso_list = [100, 200, 320, 400, 500, 640, 800]
   
if camera.iso == 100:
	iso_index = 0
elif camera.iso ==200:
	iso_index = 1
elif camera.iso ==320:
	iso_index = 2 
elif camera.iso ==400:
	iso_index = 3
elif camera.iso ==500:
	iso_index = 4
elif camera.iso ==640:
	iso_index = 5
elif camera.iso ==800:
	iso_index = 6
#####
#PhotoDirectory setup
#####
now = time.strftime("%Y-%m-%d") #get the current date and time for the start of the filename
folder_path = file_path+now
def create_folder():
	dirname = folder_path
	if not os.path.exists(dirname):
		os.makedirs(dirname)
# set variables to properly display the image on screen at right ratio
def set_demensions(img_w, img_h):
	# Note this only works when in booting in desktop mode. 
	# When running in terminal, the size is not correct (it displays small). Why?

    # connect to global vars
    global transform_y, transform_x, offset_y, offset_x

    # based on output screen resolution, calculate how to display
    ratio_h = (config.monitor_w * img_h) / img_w 

    if (ratio_h < config.monitor_h):
        #Use horizontal black bars
        #print "horizontal black bars"
        transform_y = ratio_h
        transform_x = config.monitor_w
        offset_y = (config.monitor_h - ratio_h) / 2
        offset_x = 0
    elif (ratio_h > config.monitor_h):
        #Use vertical black bars
        #print "vertical black bars"
        transform_x = (config.monitor_h * img_w) / img_h
        transform_y = config.monitor_h
        offset_x = (config.monitor_w - transform_x) / 2
        offset_y = 0
    else:
        #No need for black bars as photo ratio equals screen ratio
        #print "no black bars"
        transform_x = config.monitor_w
        transform_y = config.monitor_h
        offset_y = offset_x = 0

    # uncomment these lines to troubleshoot screen ratios
#     print str(img_w) + " x " + str(img_h)
#     print "ratio_h: "+ str(ratio_h)
#     print "transform_x: "+ str(transform_x)
#     print "transform_y: "+ str(transform_y)
#     print "offset_y: "+ str(offset_y)
#     print "offset_x: "+ str(offset_x)

# display one image on screen
def show_image(image_path):
  # clear the screen
  screen.fill( (0,0,0) )
  # load the image
  img = pygame.image.load(image_path)
  img = img.convert() 
  # set pixel dimensions based on image
  set_demensions(img.get_width(), img.get_height())
  # rescale the image to fit the current display
  img = pygame.transform.scale(img, (transform_x,transfrom_y))
  screen.blit(img,(offset_x,offset_y))
  pygame.display.flip()
##### 
#multi-threading
#####
# Define a function for the thread
def button_exit(name,delay):
   #GPIO.setmode(GPIO.BOARD)
   #GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
   global exit_status
   global GPIO
   global test_status
   global camera
   global iso_index
   
	   
   while(exit_status==False):
	   
		   
	   if(GPIO.input(DOWN_BUTTON)==1):
		   #decrease iso
		   _thread.start_new_thread( blink_thread,("exit_button",2))
		   time.sleep(BUTTON_DELAY)
		   iso_index = (iso_index -1)%7
		   camera.iso = iso_list[iso_index]	   
		   print(camera.iso)
		   time.sleep(BUTTON_DELAY)
		   
	   if(GPIO.input(UP_BUTTON)==1):
		   #increase iso
		   _thread.start_new_thread( blink_thread,("exit_button",2))
		   time.sleep(BUTTON_DELAY)
		   iso_index = (iso_index +1)%7
		   camera.iso = iso_list[iso_index]	
		   print(camera.iso)
		   time.sleep(BUTTON_DELAY)
		   
		
			   
		   

	   if(GPIO.input(EXIT_BUTTON)==1):
		   _thread.start_new_thread( blink_thread,("exit_button",2))
		   exit_status = True
#Blink in separate thread
def blink_thread(name,delay):
	GPIO.output(RED_LED,1)
	time.sleep(1)
	GPIO.output(RED_LED,0)
#PlaySound
def play_sound(name,delay):
	sound_file = sound_folder_path + name+".mp3"
	pygame.mixer.music.load(sound_file)
	pygame.mixer.music.play(0)
	time.sleep(delay)
#changeTheme
def change_theme(theme):
	if(theme=='cowboy'):
		return 'announcer'
	elif(theme == 'announcer'):
		return 'hippy'
	elif(theme =='hippy'):
		return 'cowboy'
	
#####
#main function
#####
def photo_booth():
	create_folder()
	_thread.start_new_thread( blink_thread,("exit_button",2))
	sound_count = 0
	theme = sound_array[sound_count]
	try:
		_thread.start_new_thread( button_exit,("exit_button",2))
	
		while exit_status != True:
			
			show_image(introSlide)
			#start taking pictures
			if(GPIO.input(START_BUTTON)==1):
				
				
				show_image(instructionsSlide)
				play_sound(theme+"ThreePhotos",5)
				time.sleep(2)
				
				process_pictures(theme)
				
				show_image(finishedSlide)
				_thread.start_new_thread(play_sound,(theme+"Done",6))
				theme = change_theme(theme)
				time.sleep(3)
			if(GPIO.input(TEST_BUTTON)==1):
				_thread.start_new_thread( blink_thread,("exit_button",2))
				camera.start_preview()
				time.sleep(TEST_DELAY)
				camera.stop_preview()
			
		
		
		
		
	except KeyboardInterrupt:
		cleanup()
		
	finally:
		cleanup()
#take a picture
def take_picture(theme):
	show_image(slide3)
	time.sleep(.5)
	show_image(slide2)
	time.sleep(.5)
	show_image(slide1)
	time.sleep(.5)
	now = time.strftime("%Y-%m-%d-%H-%M-%S")
	picname=now+".jpg"
	picname_resize = now + "_border.jpg"
	
	filename = folder_path +"/"+picname
	filename_resize = folder_path +"/"+picname_resize
	camera.start_preview()
	time.sleep(.5)
	_thread.start_new_thread( blink_thread,("exit_button",2))
	#GPIO.output(RED_LED,1)
	_thread.start_new_thread( play_sound,("cameraShutter1",2))
	camera.capture(filename)
	#resize to 1200X720
	os.system("gm convert " +filename +" -resize 1200X720\! "+ filename_resize)
	os.system("gm convert -background white -bordercolor white "+ filename_resize+ " -border 10%x10% "+filename_resize)
	#GPIO.output(RED_LED,0)
	clear_screen()
	camera.stop_preview()	
	
	return picname_resize
	
def process_pictures(theme):
	list_of_pics = ""
	result_file_name = folder_path +"/result_"+time.strftime("%Y-%m-%d-%H-%M-%S") + ".jpg"
	_thread.start_new_thread( play_sound,(theme +"LookAtCamera",5))
	for i in range(NUMBER_OF_PICS):
		list_of_pics = folder_path+ "/"+take_picture(theme) + " "+list_of_pics
		time.sleep(1)
	print(list_of_pics)
	print(folder_path)
	os.system("cd " + folder_path)
	os.system("pwd")
	os.system("gm convert "+list_of_pics+" -append "+ result_file_name)
	#print stuff here
	show_image(processingSlide)
	_thread.start_new_thread(play_sound,(theme+"Prints",6))
	if(PRINTER_ENABLED==True):
		os.system("lp " +result_file_name+" -d "+ PRINTER_NAME)
	time.sleep(10)
				
# display a blank screen
def clear_screen():
	screen.fill( (0,0,0) )
	pygame.display.flip()		
#cleanup
def cleanup():
	print('Ended abruptly')
	pygame.quit()
	GPIO.cleanup()
	#remove border files
	#print("Removing temporary files...")
	#os.system("rm *_border.jpg")
	print("Exiting... Down")
	if(SHUTDOWN_ENABLED == True):
		print("Shutting Down")
		os.system("sudo shutdown -h now")
  
#run the main program: photo_booth() 
photo_booth()
