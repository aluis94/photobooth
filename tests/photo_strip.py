import os
result_file_name = "result_resize.jpg"
list_of_pics = ["alien_resize","bridge_resize","duck_resize"]
pic_list_string = "alien_resize_border.jpg bridge_resize_border.jpg duck_resize_border.jpg"
printer_name = "HP_DeskJet_1110_series_USB_CN7CJ281Q8065R_HPLIP"

try:
	#crop all images
	os.system("gm convert -background white -bordercolor white "+ list_of_pics[0]+".jpg"+ " -border 10%x10% "+list_of_pics[0]+"_border.jpg")
	os.system("gm convert -background white -bordercolor white "+ list_of_pics[1]+ ".jpg"+" -border 10%x10% "+list_of_pics[1]+"_border.jpg")
	os.system("gm convert -background white -bordercolor white "+ list_of_pics[2]+ ".jpg"+" -border 10%x10% "+list_of_pics[2]+"_border.jpg")

	#create single image
	os.system("gm convert "+pic_list_string+" -append "+ result_file_name)
	os.system("lp -o scaling=100 " +result_file_name+" -d "+ printer_name)
except:
	pass
	
