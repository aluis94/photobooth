#import the GPIO and time package
import RPi.GPIO as GPIO
import time
import _thread

#constants
RED_LED = 7
START_BUTTON = 12
EXIT_BUTTON = 16
TEST_BUTTON =11 
UP_BUTTON =13
DOWN_BUTTON =15
BUTTON_DELAY = 0.25
LED_DELAY = 0.25

GPIO.setmode(GPIO.BOARD)
GPIO.setup(RED_LED, GPIO.OUT)
GPIO.setup(START_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(EXIT_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(TEST_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(UP_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(DOWN_BUTTON, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

exit_status = False
blink_status = False

#seperate thread function:
def blink_exit(name,delay):
   #GPIO.setmode(GPIO.BOARD)
   #GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
   global exit_status
   global GPIO
   global blink_status
   
   while(exit_status==False):
	   #blink LED
	   if(blink_status ==True):
		   GPIO.output(RED_LED,1)
		   time.sleep(LED_DELAY)
		   GPIO.output(RED_LED,0)
		   time.sleep(LED_DELAY)
	       
	   if(GPIO.input(EXIT_BUTTON)==1):
	      #print("button_exit")
	      exit_status = True
	      blink_status = False
try:
	_thread.start_new_thread( blink_exit,("exit_button",2))
	print("Board test")
	print("TESTING LED...")
	while(exit_status == False):
		if(GPIO.input(START_BUTTON)== 1):
			time.sleep(BUTTON_DELAY)
			print("Start Button")
			blink_status = True
			
		if(GPIO.input(TEST_BUTTON) == 1):
			time.sleep(BUTTON_DELAY)
			print("TEST Button")
			blink_status = False
			
		if(GPIO.input(UP_BUTTON) == 1):
			time.sleep(BUTTON_DELAY)
			print("UP Button")
			LED_DELAY = LED_DELAY + .1
			
		if(GPIO.input(DOWN_BUTTON) == 1):
			time.sleep(BUTTON_DELAY)
			print("DOWN Button")
			if(LED_DELAY >.1):
				LED_DELAY = LED_DELAY - .1
			if(LED_DELAY <0):
				LED_DELAY = 0.001
	print("exiting...")
	GPIO.cleanup()
except KeyboardInterrupt:
	GPIO.cleanup()
	
	
