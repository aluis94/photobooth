#import the GPIO and time package
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(12, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)

try:
	print("Press the button to light the LED...")
	while GPIO.input(16)!=1:
		if(GPIO.input(12)==1):
			GPIO.output(7,1)
			
		else:
			GPIO.output(7,0)
	GPIO.cleanup()
	print("Exiting...")
except KeyboardInterrupt:
	GPIO.cleanup()
	
