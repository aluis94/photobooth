#!/usr/bin/python
#multithreading with gpio button and LED
import _thread
#import the GPIO and time package
import RPi.GPIO as GPIO
import time
GPIO.setmode(GPIO.BOARD)
GPIO.setup(7, GPIO.OUT)
GPIO.setup(12, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
exit_status = False
# Define a function for the thread
def button_exit(name,delay):
   #GPIO.setmode(GPIO.BOARD)
   #GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
   global exit_status
   global GPIO
   while(exit_status==False):
	   
	   if(GPIO.input(16)==1):
		   #print("button_exit")
		   
		   exit_status = True
   
try:
	
	_thread.start_new_thread( button_exit,("exit_button",2))
	print("Button will blink until button is pressed...")
	while exit_status!=True:
		GPIO.output(7,0)
		time.sleep(.5)
		GPIO.output(7,1)
		time.sleep(.5)
	print("Cleaning up")
	GPIO.cleanup()
	print("Exiting...")
except KeyboardInterrupt:
	GPIO.cleanup()



# Create two threads as follows
#try:
#   thread.start_new_thread( print_time, ("Thread-1", 2, ) )
 #  thread.start_new_thread( print_time, ("Thread-2", 4, ) )
