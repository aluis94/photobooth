#Config settings to change behavior of photo booth
NUMBER_OF_PICS = 3 #change this for the number of pics taken
monitor_w = 800    # width of the display monitor
monitor_h = 480    # height of the display monitor
file_path = '/home/pi/workspace/photobooth/pics/' # path to save images
clear_on_startup = False # True will clear previously stored photos as the program launches. False will leave all previous photos.
debounce = 0.3 # how long to debounce the button. Add more time if the button triggers too many times.
post_online = True # True to upload images. False to store locally only.
capture_count_pics = True # if true, show a photo count between taking photos. If false, do not. False is faster.
make_gifs = True   # True to make an animated gif. False to post 4 jpgs into one post.
hi_res_pics = False  # True to save high res pics from camera.
                    # If also uploading, the program will also convert each image to a smaller image before making the gif.
                    # False to first capture low res pics. False is faster.
                    # Careful, each photo costs against your daily Tumblr upload max.
camera_iso = 0   # adjust for lighting issues. Normal is 100 or 200. Sort of dark is 400. Dark is 800 max.
                    # available options: 100, 200, 320, 400, 500, 640, 800
#path to slide images
slidesFolder = './slides/'
introSlide = slidesFolder + 'intro.png'
instructionsSlide = slidesFolder + 'instructions.png'
processingSlide = slidesFolder + 'processing.png'
finishedSlide = slidesFolder + 'finished2.png'
slide1 = slidesFolder + 'pose1.png'
slide2 = slidesFolder + 'pose2.png'
slide3 = slidesFolder + 'pose3.png'
slide4 = slidesFolder + 'pose4.png'
#sound folder
sound_folder_path = '/home/pi/workspace/photobooth/sounds/'
#GPIO setup
RED_LED = 7
START_BUTTON = 12
EXIT_BUTTON = 16
TEST_BUTTON = 11
UP_BUTTON = 13
DOWN_BUTTON = 15
#shutdown
SHUTDOWN_ENABLED = False
#printer
PRINTER_ENABLED = False
PRINTER_NAME = "HP_DeskJet_1110_series_USB_CN7CJ281Q8065R_HPLIP"

